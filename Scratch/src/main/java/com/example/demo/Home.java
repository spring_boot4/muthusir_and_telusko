package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component

public class Home {  //pojo class
private String Owner;
private int dooNo;

@Autowired
@Qualifier("abc")
private InternetConnection modem;
public String getOwner() {
	return Owner;
}
public void setOwner(String owner) {
	Owner = owner;
}
public int getDooNo() {
	return dooNo;
}
public void setDooNo(int dooNo) {
	this.dooNo = dooNo;
}

public void connect() {
	modem.swithchOn();
	System.out.println("connecting to internet");
}
}
