package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication

public class ScratchApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context=
				SpringApplication.run(ScratchApplication.class, args);
		Home h=context.getBean(Home.class);
		h.connect();
	}

}
