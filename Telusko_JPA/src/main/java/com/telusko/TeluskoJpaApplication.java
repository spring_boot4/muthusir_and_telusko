package com.telusko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeluskoJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeluskoJpaApplication.class, args);
	}

}
