package com.telusko.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.telusko.model.Alien_model;

public interface AlienRepo extends JpaRepository<Alien_model,Integer> {

	List<Alien_model> findByTech(String tech);
}
