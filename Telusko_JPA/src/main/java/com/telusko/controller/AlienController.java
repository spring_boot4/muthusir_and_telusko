package com.telusko.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.telusko.dao.AlienRepo;
import com.telusko.model.Alien_model;

@Controller
public class AlienController {

	@Autowired
	AlienRepo repo;
	@RequestMapping("/home")
	public String home() {
		return "home.jsp";
	}
	
	@RequestMapping("/addAlien")
	public String addAlien(Alien_model entity) {
		
		repo.save(entity);
		return "home.jsp";
	}
	
	
	
//	@RequestMapping("/getAlien")
//	public ModelAndView getAlien(@RequestParam int aid) {
//		
//		ModelAndView mv=new ModelAndView("show.jsp");
//		Alien_model entity=repo.findById(aid).orElse(new Alien_model());
//		mv.addObject(entity);
//		mv.addObject("data", entity);
//		//System.out.println("callinnggggg");
//		return mv;
//	}
	
	@RequestMapping("/Alien/{aid}")   // http://localhost:8080/Alien/1
	@ResponseBody
	public Optional<Alien_model> getAlien(@PathVariable("aid") int aid) {
		
		//return repo.findById(aid).toString();
		return repo.findById(aid);
	
	}
	@RequestMapping("/allAlien")   // http://localhost:8080/allAlien
	@ResponseBody
	public List<Alien_model> allAlien() {
		
		//return repo.findAll().toString();
		return repo.findAll();
	}
	
}
