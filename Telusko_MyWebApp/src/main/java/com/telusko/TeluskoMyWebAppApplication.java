package com.telusko;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeluskoMyWebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeluskoMyWebAppApplication.class, args);
	}

}
