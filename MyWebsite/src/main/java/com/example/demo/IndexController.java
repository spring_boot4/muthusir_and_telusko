package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

@Controller
public class IndexController {

//	@RequestMapping("index")
//	@ResponseBody
//	public String index() {
//	
//		//System.out.println("hello");
//	return "hello"; 
//}
	
//	@RequestMapping("index")
//	
//	public String index(HttpServletRequest request) {
//	
//		String s=request.getParameter("name");
//		HttpSession session=request.getSession();
//		session.setAttribute("username", s);
//		//System.out.println("hello");
//	return "index"; 
//}
	
//	@RequestMapping("index")
//	
//	public String index(@RequestParam("name") String uname,HttpSession session) {  //controller
//	
//		
//		session.setAttribute("username", uname);  //data->model
//		//System.out.println("hello");
//	return "index";   // view
//}	

	
//@RequestMapping("index")
//	
//	public ModelAndView index(@RequestParam("name") String uname,HttpSession session) {  
//	
//		
//	ModelAndView mav=new ModelAndView();
//	mav.addObject("username",uname);
//	mav.setViewName("index");
//		session.setAttribute("username", uname);  
//		
//	return mav;   
//}	

@RequestMapping("index")

public ModelAndView index(UserDetails ud) {  

	
ModelAndView mav=new ModelAndView();
mav.addObject("details",ud);
mav.setViewName("index");
	
	
return mav;   
}	
}


