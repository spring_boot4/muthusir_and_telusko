package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Telusko2Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext  context=SpringApplication.run(Telusko2Application.class, args);
		Alien a=context.getBean(Alien.class);
		a.show();
		
//		Alien a1=context.getBean(Alien.class);
//		a1.show();
	}

}
